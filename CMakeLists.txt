cmake_minimum_required(VERSION 3.5)

SET(CMAKE_BUILD_TYPE "Debug")

project(eda_dream)

file(GLOB source_files "src/*.cc")

set(SOURCES
    ${source_files}
)

add_executable(eda_dream ${SOURCES})

target_include_directories(eda_dream
    PRIVATE
    ${PROJECT_SOURCE_DIR}/include
)