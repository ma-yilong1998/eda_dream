# Graph basic

## 1 Definition and terminology

1 

graph : $G = (V, E)$

$V$ represents vertex, $E$ represents edge.

$V$ is ==finite and non-empty==;

$E$ is ==finite==.



2

$G_1$ is **directed graph**, which consists of 4 vertices and 4 edges.

$G_2$ is **undirected graph**, consists of 5 vertices and 7 edges.

![image-20210716195610899](.\PNG\1\image-20210716195610899.png)



3

**Complete graph** : any two points are connected by an edge.

![image-20210716201616078](.\PNG\1\image-20210716201616078.png)

if directed complete graph has n vertices, it has $\frac{n(n-1)}{2}$edges;

if undirected complete graph has n vertices, it has ${n(n-1)}$edges.



4

Sparse graph : edge number $\lt nlogn$;

Dense graph : edge number $\ge nlogn$.



5

Net : graph's edge with weights.



6

Adjacency : it describe two vertices which is connected.

> $(x, y)$ means $x$, $y$ adjacent to each other;
>
> $<x, y>$ means $x$ is adjacent to $y$.



7

Vertex degree $TD(v)$ : Number of edges which connect same vertex.

> In directed graph, the degree subdivided into in-degree and out-degree.
>
> in-degree $ID(v)$ : o←
>
> out-degree $OD(v)$ : o→



8

Directed tree : In this directed graph, one vertex(root) in-degree is $0$ and other vertex in-degree is $1$.

![image-20210716204214964](.\PNG\1\image-20210716204214964.png)



****

9

Path : sequence of vertices formed by consecutive edges.



10

Path length : summary of edge or weights(in net) in path.



<img src=".\PNG\1\path & path length.png" alt="path & path length" style="zoom:28%;" />



11

Loop : The start vertex is same as the end vertex in path.



12

Simple path : except that the start vertex and the end vertex can be same, in path, the other vertices are different.

Simple loop : simple path + start and end must be same.



****

13

Connected graph : Any two vertices($a,b\in V$) exist a path from $a$ to $b$.

Strongly connected graph : connected graph in directed graph.

![image-20210716211101891](.\PNG\1\image-20210716211101891.png)



14

Subgraph : $G = (V, E)$ & $G_1 = (V_1, E_1)$ & $V_1\subseteq V$ & $E_1\subseteq E$, then $G_1$ is a subgraph of $G$.



15*

Connected component : undirected graph's ==maximal connected subgraph== is called connected component.

> Connected subgraph : the subgraph is also a connected graph;
>
> Maximal connected subgraph : $G_1$ is an connected subgraph of $G$, when adding any vertex in $G_1$(the vertex is in $G$), $G_1$ is not connected. $G_1$ is called maximal connected subgraph.
>
> maximal connected subgraph = connected component

<img src=".\PNG\1\image-20210716213716690.png" alt="image-20210716213716690" style="zoom:97%;" />

Strongly connected component : connected component in directed graph.

![image-20210716214353135](.\PNG\1\image-20210716214353135.png)



16

Minimal connected subgraph : $G_1$ is an connected subgraph of $G$, when ==deleting== any vertex in $G_1$, $G_1$ is not connected. $G_1$ is called minimal connected subgraph.

Spanning tree : ==the minimal connected== subgraph which contain all vertices in $G$.



## 2 Graph definition

Object $V$(vertex set) : a set of elements with same characteristics.

Relationship $R$ : $R = \left\{ VR\right\}$

$VR = <v,w> | v,w\in V \and p(v,w)$

$<v,w>$ is an edge v to w,

 $p(v,w)$ represents the message(such as weights) on edge.



basic operate $P$:

- CreateGraph() : create an empty graph ;
- CreateGraph(&G, V, VR) : create an graph $G$ followed by $V$ & $VR$ ;
- GetVertex(G, v) : get value in vertex $v$ ;
- DFSTraverse(G) : depth first search ; *
- BFSTraverse(G) : breadth first search ; *
- ...



## 3 Data structure of graph

### How to describe a graph

- **Adjacency matrix**(two-dimensional array) ;  *
- Multiple list : 
  - **Adjacency list** ;  *
  - Adjacency multiple list ;
  - Cross list ;



### Adjacency matrix

**Vertex table** $Vexs[n]$ record information about every vertex ; 

**adjacency matrix** $arcs[n][n]$ describes relationship between each vertex.
$$
arcs[i][j]=\begin{cases}
1, & if\quad <i, j>\in VR\\
0, & otherwise
\end{cases}
,\quad i,j \in V
$$

**Example** :

<img src=".\PNG\3\undirected_graph_adjacency_array.png" alt="undirected_graph_adjacency_array" style="zoom:50%;" />

<img src=".\PNG\3\directed_graph_adjacency_array.png" alt="directed_graph_adjacency_array" style="zoom:50%;" />

adjacency matrix: 

- symmetric matrix ;
- the main diagonal elements are all zero ;
- (in undirected graph) degree $TD(v)=\sum ^{n-1} _{i=0} {arcs[v][i]}$ ;
- (in directed graph) :
  - out-degree $OD(v)=\sum^{n-1}_{i=0}arcs[v][i]$ ;
  - in-degree     $ID(v)=\sum^{n-1}_{j=0}arcs[j][v]$ ;



**Adjacency array in Net(graph with weights)**
$$
arcs[i][j]=\begin{cases}
w_{ij}, & if\quad <i, j>\in VR\\
\infty, & otherwise
\end{cases}
,\quad i,j \in V
$$

**Example** :

<img src=".\PNG\3\net_adjacency_array.png" alt="net_adjacency_array" style="zoom:50%;" />



### How to create an adjacency array

details are in `/Eda_dream/src/` & `/Eda_dream/include/`

```cpp
class UndirectedNet {
  public:
    UndirectedNet();
    UndirectedNet(Uint32 v, Uint32 e);
    UndirectedNet(const UndirectedNet &net);
    UndirectedNet &operator=(const UndirectedNet &net);
    ~UndirectedNet();
    
  private:
    Uint32 vertex_num_;
    Uint32 edg_num_;
    std::vector<char> *vertex_table_;
    std::vector<std::vector<int> > *adjacency_matrix_;

  private:
    static Uint32 getINF() {
        static Uint32 INTINF = 0xffffffff;
        return INTINF;
    }
    void init(const UndirectedNet &net);

  public:
    void printVT() const;
};
```

