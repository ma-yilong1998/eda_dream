#include "maze.h"

#include <fstream>
#include <iostream>

namespace eda_dream {
namespace graph {

Maze::Maze(const char *file_path) {
    std::ifstream ifs(file_path);
    ifs >> row_ >> col_;
    table_ = std::vector<std::vector<Grid> > (row_, 
        std::vector<Grid>(col_)
    );
    char c;
    for (int i = 0; i < row_; ++i)
        for (int j = 0; j < col_; ++j) {
            table_[i][j].SetPoint(Point(i, j));
            ifs >> c;
            switch (c){
                case 's' : 
                    table_[i][j].SetGridType(kSource);
                    src_ = Point(i, j);
                    break;
                case 'd' : 
                    table_[i][j].SetGridType(kDestination);
                    dst_ = Point(i, j);
                    break;
                case '*' : 
                    table_[i][j].SetGridType(kPath);
                    break;
                case 'x' : 
                    table_[i][j].SetGridType(kObstacle);
                    break;
                default : ;
            }
        }
}

void Maze::printMaze() const {
    for (int i = 0; i < row_; ++i) {
        for (int j = 0; j < col_; ++j) {
            std::cout.width(2);
            switch (table_[i][j].GetGridType()) { 
                case kSource :
                    std::cout << 'S';
                    break;
                case kDestination : 
                    std::cout << 'D';
                    break;
                case kPath :
                    std::cout << table_[i][j].GetStep();
                    break;
                case kObstacle :
                    std::cout << 'X';
                    break;
                default :
                    std::cout << '?';
            }
            std::cout << ' ';
        }
        std::cout << "\n";
    }
    std::cout << "-----------------------------------------------" << std::endl;
}

}   // namespace graph
}   // namespcace eda_dream