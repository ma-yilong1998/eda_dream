#include "maze.h"
#include "eda_dream.h"
#include "dfs.h"

using Maze = eda_dream::graph::Maze;
int main() {
    auto &maze = Maze::getMaze("/home/mayl/eda_dream/graph");
    maze.printMaze();
    eda_dream::graph::algorithm::DepthFirstSearch(maze);

    return 0;
}