#include "udn.h"

#include "stdio.h"

#include <iostream>

namespace eda_dream {
namespace graph {

UndirectedNet::UndirectedNet() : 
    AMGraph()
{ }

UndirectedNet::UndirectedNet(Uint32 v, Uint32 e) : 
    AMGraph(v, e)
{
    int row, col;
    vertex_table_ = new std::vector<VerType>(v);
    adjacency_matrix_ = new std::vector<std::vector<Uint32> >(v, std::vector<Uint32>(v));
    for (Uint32 i = 0; i < v; ++i) {
        std::cin >> (*vertex_table_)[i];
    }
    // initialize 
    for (Uint32 i = 0; i < v; ++i) {
        for (Uint32 j = 0; j < v; ++j) {
            (*adjacency_matrix_)[i][j] = getINF();
        }
    }
    for (Uint32 i = 0; i < e; ++i) {
            std::cin >> row >> col;
            std::cin >> (*adjacency_matrix_)[row][col];
            (*adjacency_matrix_)[col][row] = (*adjacency_matrix_)[row][col];
    }
}

UndirectedNet::UndirectedNet(const UndirectedNet &net) :
    AMGraph(net.vertex_num_, net.edge_num_)
{ 
    Copy(net);
}

UndirectedNet &UndirectedNet::operator=(const UndirectedNet &net) {   
    AMGraph(net.vertex_num_, net.edge_num_);
    Copy(net);
    return *this;
}

UndirectedNet::~UndirectedNet() { }

void UndirectedNet::printAM() const {
    for (Uint32 i = 0; i < vertex_num_; ++i) {
        for (Uint32 j = 0; j < vertex_num_; ++j) {
            if ((*adjacency_matrix_)[i][j] != getINF())
                printf("%5d", (*adjacency_matrix_)[i][j]);
            else
                printf("%5s", "inf");
        }
        std::cout << "\n";
    }
    std::cout << std::endl;
}

}   // namespace graph
}   // namespcace eda_dream

