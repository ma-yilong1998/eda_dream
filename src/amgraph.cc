#include "amgraph.h"

namespace eda_dream {
namespace graph {

AMGraph::AMGraph() :
    vertex_num_(0), 
    edge_num_(0),
    vertex_table_(nullptr),
    adjacency_matrix_(nullptr)
{ }

AMGraph::AMGraph(Uint32 v, Uint32 e) :
    vertex_num_(v), 
    edge_num_(e),
    vertex_table_(nullptr),
    adjacency_matrix_(nullptr)
{ }

AMGraph::AMGraph(const AMGraph &g) :
    vertex_num_(g.vertex_num_), 
    edge_num_(g.edge_num_),
    vertex_table_(nullptr),
    adjacency_matrix_(nullptr)
{
    Copy(g);
}

AMGraph &AMGraph::operator=(const AMGraph &g) {
    AMGraph(g.vertex_num_, g.edge_num_);
    Copy(g);

    return *this;
}

AMGraph::~AMGraph() {
    delete vertex_table_;
    delete adjacency_matrix_;
}

void AMGraph::Copy(const AMGraph &g) {
    std::vector<VerType> *pVT = vertex_table_;
    std::vector<std::vector<Uint32> > *pAM = adjacency_matrix_;

    vertex_table_ = new std::vector<VerType>(*(g.vertex_table_));
    adjacency_matrix_ = new std::vector<std::vector<Uint32> >(*(g.adjacency_matrix_));

    delete pVT;
    delete pAM;
}

} // namespace graph
} // namespace eda_dream