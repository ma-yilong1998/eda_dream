#ifndef POINT_H__
#define POINT_H__

#include "type.h"

namespace eda_dream {
namespace graph {

class Point {
  public:
    Point() = default;
    Point(Coord x, Coord y) : x_(x), y_(y) {}
    Point(const Point &point) : x_(point.GetX()), y_(point.GetY()) {}
    Point &operator=(const Point &point) {
        x_ = point.x_;
        y_ = point.y_;
        return (*this);
    }

    Coord GetX() const { return x_; }
    void SetX(Coord x) { x_ = x; }
    Coord GetY() const { return y_; }
    void SetY(Coord y) { y_ = y; }

  private:
    Coord x_ = 0;
    Coord y_ = 0;
};

}
}

#endif // POINT_H__