#ifndef AMGRAPH_H__
#define AMGRAPH_H__

#include <vector>

#include "type.h"

namespace eda_dream {
namespace graph {

// Define an adjacency matrix graph
class AMGraph {
  public:
    AMGraph();
    AMGraph(Uint32 v, Uint32 e);
    AMGraph(const AMGraph &g);
    AMGraph &operator=(const AMGraph &g);
    ~AMGraph();
  
  protected:
    Uint32 vertex_num_;
    Uint32 edge_num_;
    std::vector<VerType> *vertex_table_;
    std::vector<std::vector<Uint32> > *adjacency_matrix_;

  protected:
    void Copy(const AMGraph &g);
};

} // namespace graph
} // namespace eda_dream

#endif // AMGRAPH_H__