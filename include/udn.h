#ifndef UDN_H__
#define UDN_H__

#include <vector>

#include "amgraph.h"

namespace eda_dream {
namespace graph {

class UndirectedNet : public AMGraph {
  public:
    UndirectedNet();
    UndirectedNet(Uint32 v, Uint32 e);
    UndirectedNet(const UndirectedNet &net);
    UndirectedNet &operator=(const UndirectedNet &net);
    ~UndirectedNet();

  private:
    static Uint32 getINF() {
        static Uint32 INTINF = 0xffffffff;
        return INTINF;
    }

  public:
    void printAM() const;
};

}   // namespace graph
}   // namespcace eda_dream

#endif // UDN_H__