#ifndef DFS_H__
#define DFS_H__

#include "maze.h"

#include <stack>

namespace eda_dream {
namespace graph {
namespace algorithm {

void DepthFirstSearch(Maze &maze);


} // namespace algorithm
} // namespace graph
} // namespace eda_dream

#endif // DFS_H__