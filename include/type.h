#ifndef TYPE_H__
#define TYPE_H__

#include <cstdint>

namespace eda_dream {
namespace graph {

enum DirectionType {
    kUp = 0,
    kDowm,
    kLeft,
    kRight,
    kUnknowDirection
};

using Uint32 = uint32_t;
using VerType = uint8_t;
using Coord = int32_t;

}   // namespace graph
}   // namespcace eda_dream

#endif // TYPE_H__