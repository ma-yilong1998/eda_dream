#ifndef MAZE_H__
#define MAZE_H__

#include <vector>

#include "point.h"

namespace eda_dream {
namespace graph {

enum GridType {
    kSource = 0,
    kDestination,
    kPath,
    kObstacle,
    kUnknowType
};

class Grid {
  public:
    Grid() = default;

    Uint32 GetStep() const { return step_; }
    void SetStep(Uint32 step) { step_ = step; }
    GridType GetGridType() const { return type_; }
    void SetGridType(GridType type) { type_ = type; }
    Point GetPoint() const { return point_; }
    void SetPoint(const Point &point) { point_ = point; }

  private:
    Uint32 step_ = 0;
    Point point_;
    GridType type_ = kUnknowType;
};

class Maze {
  private:
    Maze(const char *file_path);
    Maze(const Maze &m) = delete;
  
  public:
    static Maze &getMaze(const char *file_path = nullptr) {
        static Maze maze(file_path);
        return maze;
    }
    std::vector<std::vector<Grid> > &GetTable() { return table_; }
    Uint32 GetRow() const { return row_; }
    Uint32 GetCol() const { return col_; }
    Point GetSource() const { return src_; }
    Point GetDestiation() const { return dst_; }

    void printMaze() const;

  private:
    std::vector<std::vector<Grid> > table_;
    Uint32 row_, col_;
    Point src_;
    Point dst_;
};

}   // namespace graph
}   // namespace eda_dream
#endif // MAZE_H__