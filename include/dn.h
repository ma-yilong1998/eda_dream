#ifndef DN_H__
#define DN_H__

#include <vector>

#include "amgraph.h"

namespace eda_dream {
namespace graph {

class DirectedNet : public AMGraph {
  public:
    DirectedNet();
    DirectedNet(Uint32 v, Uint32 e);
    DirectedNet(const DirectedNet &net);
    DirectedNet &operator=(const DirectedNet &net);
    ~DirectedNet();

  private:
    static Uint32 getINF() {
        static Uint32 INTINF = 0xffffffff;
        return INTINF;
    }

  public:
    void printAM() const;
};

}   // namespace graph
}   // namespcace eda_dream

#endif // DN_H__