#!/bin/bash

[ -d ./build ] && rm -rf ./build
[ -d ./output ] && rm -rf ./output

mkdir ./build && cd ./build
cmake .. 

# window
# MSBuild.exe eda_dream.sln

# linux
make -j 32

cd .. && mkdir -p ./output/bin/ && cp ./build/eda_dream ./output/bin/

echo "Build eda_dream successfully."
